# App Produtos

A new Flutter project.

## Flutter
## Dart
## Firebase

- Login/Cadastro

- Adicionar/Remover produto

- Buscar produto

- Adicionar/remover imagem (Câmera/Galeria)

<img height="480px" src="screenshots/0.png"> <img height="480px" src="screenshots/01.png"><img height="480px" src="screenshots/1.png"> <img height="480px" src="screenshots/2.png"> <img height="480px" src="screenshots/3.png"> <img height="480px" src="screenshots/4.png"><img height="480px" src="screenshots/apagar_produto.png"><img height="480px" src="screenshots/apagar_produto.png"> <img height="480px" src="screenshots/5.png"> <img height="480px" src="screenshots/6.png"> <img height="480px" src="screenshots/7.png"><img height="480px" src="screenshots/8.png">


Desafio Mobile - Flutter - Cadastro de produtos

Desenvolva um aplicativo em Flutter (com suporte para Android e iOS), onde o mesmo

atenda os seguintes pré-requisitos:

-  Possua uma interface de login;

-  Possua uma interface de cadastro.

-  Caso o usuário não esteja cadastrado, o mesmo deverá utilizar essa interface para realizar o cadastro.

-  Nesse cadastro as principais informações do usuário deverão ser coletadas.

-  Interface home, onde o usuário terá acesso às principais funcionalidades do aplicativo.

-  O aplicativo deverá ter uma listagem onde serão exibidos os produtos até então cadastrados.

-  O usuário poderá realizar o cadastro de produtos (o nicho dos produtos é de suaescolha).

-  As principais informações do produto deverão ser coletadas.

-  Imagens do produto poderão ser anexadas neste cadastro.

-  O usuário poderá realizar a edição de cada produto cadastrado.

- Todas as informações do produto poderão sofrer alterações.

- O aplicativo deverá permitir a exclusão dos itens já cadastrados. 

Método de avaliação
A forma de organização do projeto fica a cargo do candidato, pois a mesma também faz
parte avaliação, bem como a estrutura lógica desenvolvida, comentários, absorção sobre o
escopo apresentado
