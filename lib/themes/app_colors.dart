import 'package:flutter/cupertino.dart';

class AppColors {
  static final white = Color(0xFFFFFFFF);
  static final black = Color(0xFF000000);
  static final accent = Color(0xFF58BE3F);
  static final green = Color(0xFF58BE3F);
  static final greenDark = Color(0xFF75A645);
  static final red = Color(0xFFFF0000);
  static final greenLigth = Color(0xFFFAFDF9);
  static final blackTextBold = Color(0xFF3A3737);
  static final grey = Color(0xFF909090);
  static final greyBaseBoard = Color(0xFFC4C4C4);
  static final primaryColor = Color(0xff0E6C79);
  static final blue = Color(0xFF3F4CBE);
  static final blueLigth = Color(0xFF5373C6);
  static final shadow = Color(0xFF000000).withOpacity(0.25);
  static final lightgreen = Color(0xfffafdf9);
}
