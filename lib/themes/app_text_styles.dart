import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app_colors.dart';

class AppTextStyles {
  static final titleLogin = GoogleFonts.poppins(
    fontSize: 36,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  static final drawerSubString = GoogleFonts.poppins(
      fontSize: 40, fontWeight: FontWeight.bold, color: AppColors.primaryColor);

  static final homeTitle = GoogleFonts.poppins(
    fontSize: 25.0,
    fontWeight: FontWeight.w600,
    color: AppColors.white,
  );

  static final text = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.normal,
    color: AppColors.black,
  );

  static final textBold = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final hint = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: AppColors.grey,
  );

  static final textPrice = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w800,
    color: AppColors.primaryColor,
  );

  static final textWhite = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.white,
  );

  static final textButtonWhite = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.white,
  );

  static final titleScreen = GoogleFonts.poppins(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static final itemList = GoogleFonts.poppins(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.blackTextBold,
  );

  static final emailStyle = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: AppColors.blackTextBold,
  );
}
