import 'package:flutter/material.dart';
import 'package:productmanagerapp/themes/app_colors.dart';
import 'package:productmanagerapp/themes/app_text_styles.dart';

import 'app_bar_widget.dart';

class CustomAppBarHome extends StatelessWidget {
  final void Function() onPressedclearText;
  final void Function() onPressedLogout;
  final void Function(String) onChanged;

  const CustomAppBarHome({
    Key key,
    this.onPressedclearText,
    this.onPressedLogout,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Center(
            child: Text(
              "Produtos",
              style: AppTextStyles.homeTitle,
            ),
          ),
          color: AppColors.primaryColor,
          height: MediaQuery.of(context).size.height * 0.2,
          width: MediaQuery.of(context).size.width,
        ),
        Container(),
        Positioned(
          top: 100.0,
          left: 20.0,
          right: 20.0,
          child: Container(
            child: AppBar(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.menu),
                color: AppColors.primaryColor,
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              ),
              primary: false,
              title: TextField(
                  onChanged: onChanged,
                  autofocus: false,
                  decoration: InputDecoration(
                      hintText: "Buscar produto",
                      border: InputBorder.none,
                      hintStyle: TextStyle(color: Colors.grey))),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.clear,
                    color: AppColors.primaryColor,
                  ),
                  onPressed: onPressedclearText,
                ),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: AppColors.primaryColor,
                  ),
                  onPressed: onPressedLogout,
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

class NavegacaoTemplate extends StatelessWidget {
  final Widget conteudo;
  final String titulo;
  final Function onBack;
  final bool bigContent;
  final double margin;
  final bool editing;
  final Function deleteProduct;
  final FloatingActionButton floatingActionButton;

  NavegacaoTemplate({
    @required this.conteudo,
    this.titulo = '',
    this.margin = 5.0,
    this.onBack,
    this.bigContent = false,
    this.floatingActionButton,
    this.editing = false,
    this.deleteProduct,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: floatingActionButton,
      backgroundColor: Colors.white,
      appBar: appBarWidget(
          titulo: titulo,
          onBack: onBack,
          context: context,
          editiing: editing,
          delete: deleteProduct),
      body: conteudo,
    );
  }
}
