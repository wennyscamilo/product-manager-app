import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/user/usuario_manager.dart';

import 'package:productmanagerapp/themes/app_colors.dart';
import 'package:productmanagerapp/themes/app_text_styles.dart';
import 'package:provider/provider.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Consumer<Usermanager>(
        builder: (_, userManger, __) {
          return ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text(
                  'Olá! ${userManger.usuario?.name ?? ''}',
                  style: AppTextStyles.textButtonWhite,
                ),
                accountEmail: Text(
                  userManger.usuario?.email ?? '',
                  style: TextStyle(color: AppColors.white),
                ),
                currentAccountPicture: CircleAvatar(
                    backgroundColor: AppColors.white,
                    child: Icon(
                      Icons.person,
                      color: AppColors.primaryColor,
                    )),
              ),
              GestureDetector(
                child: ListTile(
                  title: Text(
                    'Produtos',
                    style: AppTextStyles.itemList,
                  ),
                  trailing: Icon(Icons.arrow_right_alt_outlined),
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      ),
    );
  }
}
