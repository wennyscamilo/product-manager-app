import 'package:flutter/material.dart';
import 'package:productmanagerapp/themes/app_colors.dart';
import 'package:productmanagerapp/themes/app_text_styles.dart';

Widget appBarWidget({
  String titulo,
  Function onBack,
  BuildContext context,
  bool editiing,
  Function delete,
}) {
  return PreferredSize(
    preferredSize: Size.fromHeight(60),
    child: SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        color: AppColors.lightgreen,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            onBack == null
                ? Container()
                : GestureDetector(
                    onTap: onBack,
                    child:
                        Icon(Icons.arrow_back, color: AppColors.primaryColor),
                  ),
            Center(
              child: Text(
                titulo,
                style: AppTextStyles.textBold,
                textScaleFactor: 0.9,
                textAlign: TextAlign.center,
              ),
            ),
            editiing
                ? IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: AppColors.red,
                    ),
                    onPressed: delete,
                  )
                : Container(),
          ],
        ),
      ),
    ),
  );
}
