import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'text_field_container.dart';

class RoundedInput extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final FormFieldValidator<String> validator;
  final TextEditingController controller;
  final bool enabled;
  final bool obscureText;
  final Function onPressed;
  final String hintText;
  final IconData iconData;
  final List<TextInputFormatter> inputFormatters;
  final bool isInputSenha;
  final TextInputType keyboardType;
  final void Function(String) onSaved;
  const RoundedInput(
      {Key key,
      this.onChanged,
      this.validator,
      this.enabled,
      this.inputFormatters,
      this.controller,
      this.obscureText = false,
      this.hintText,
      this.iconData,
      this.onSaved,
      this.isInputSenha = false,
      this.keyboardType,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        onSaved: onSaved,
        obscureText: obscureText,
        onChanged: onChanged,
        inputFormatters: inputFormatters,
        autocorrect: false,
        cursorColor: Theme.of(context).primaryColor,
        validator: validator,
        keyboardType: keyboardType,
        enabled: enabled,
        controller: controller,
        decoration: InputDecoration(
          isDense: true,
          filled: true,
          hintText: hintText,
          prefixIcon:
              iconData != null ? Icon(iconData, color: Colors.grey) : null,
          contentPadding: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          suffixIcon: isInputSenha
              ? IconButton(
                  icon: Icon(Icons.visibility),
                  color: Theme.of(context).primaryColor,
                  onPressed: onPressed,
                )
              : Container(
                  width: 0,
                ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }
}
