import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/product/product.dart';

import 'package:productmanagerapp/screens/base/base_screen.dart';

import 'package:productmanagerapp/screens/login/login_screen.dart';
import 'package:productmanagerapp/screens/product/edit_product/edit_product_screen.dart';
import 'package:productmanagerapp/screens/signup/signup_screen.dart';

abstract class Routes {
  static String initialRoute = '/base';
  static Route<MaterialPageRoute> onGerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/login':
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => LoginScreen(),
        );

      case '/edit_product':
        return MaterialPageRoute(
          builder: (_) => EditProductScreen(settings.arguments as Product),
        );

      case '/cadastro':
        return MaterialPageRoute(
          builder: (_) => SingupScreen(),
        );

      case '/base':
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => BaseScreen(),
        );
    }
  }
}
