import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:productmanagerapp/routes/routes.dart';
import 'package:productmanagerapp/themes/app_colors.dart';
import 'package:provider/provider.dart';

import 'models/product/product_manager.dart.dart';
import 'models/user/usuario_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => Usermanager(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => ProductManager(),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          textTheme: GoogleFonts.poppinsTextTheme(),
          primaryTextTheme: GoogleFonts.poppinsTextTheme(),
          accentTextTheme: GoogleFonts.poppinsTextTheme(),
          primaryColor: AppColors.primaryColor,
          accentColor: AppColors.green,
          backgroundColor: AppColors.white,
          scaffoldBackgroundColor: AppColors.white,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        onGenerateRoute: Routes.onGerateRoute,
      ),
    );
  }
}
