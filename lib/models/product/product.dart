import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

class Product extends ChangeNotifier {
  String id;
  String name;
  String description;
  List<String> images;
  List<dynamic> newImages;
  num price;
  int stock;

  Product({
    this.id,
    this.name,
    this.description,
    this.images,
    this.price,
    this.stock,
  }) {
    images = images ?? [];
  }

  factory Product.fromDocument(
      DocumentSnapshot<Map<String, dynamic>> document) {
    return Product(
      id: document.id,
      name: document['name'] as String,
      description: document['description'] as String,
      price: document['price'] as num,
      images: List<String>.from(document['images'] as List<dynamic>),
      stock: document['stock'] as int,
    );
  }
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  final FirebaseStorage storage = FirebaseStorage.instance;

  DocumentReference get firestoreRef => firebaseFirestore.doc('produtos/$id');
  Reference get storageRef => storage.ref().child('produtos').child(id);

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }

  Future<void> save() async {
    loading = true;
    final Map<String, dynamic> data = {
      'name': name,
      'description': description,
      'price': price,
      'stock': stock
    };
    if (id == null) {
      final doc = await firebaseFirestore.collection('produtos').add(data);
      id = doc.id;
    } else {
      await firestoreRef.update(data);
    }

    final List<String> updateImages = [];
    for (final newImage in newImages) {
      if (images.contains(newImage)) {
        updateImages.add(newImage as String);
      } else {
        final UploadTask task =
            storageRef.child(Uuid().v1()).putFile(newImage as File);
        final TaskSnapshot snapshot = await task; //.onComplete;
        final String url =
            await (await snapshot).ref.getDownloadURL() as String;
        updateImages.add(url);
      }
    }
    for (final image in images) {
      if (!newImages.contains(image) && image.contains('firebase')) {
        try {
          final ref = storage.refFromURL(image);
          await ref.delete();
        } catch (e) {
          debugPrint('falha ao delelar $image');
        }
      }
    }
    await firestoreRef.update({'images': updateImages});
    images = updateImages;
    loading = false;
  }

  Future<void> delete(Product produto) async {
    firebaseFirestore.collection('produtos').doc(produto.id).delete();
    notifyListeners();
  }

  Product clone() {
    return Product(
      id: id,
      name: name,
      description: description,
      images: List.from(images),
      price: price,
      stock: stock,
    );
  }

  @override
  String toString() {
    return 'Produto{id: $id, name: $name, description: $description, images: $images,  newImages: $newImages}';
  }
}
