import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:productmanagerapp/models/product/product.dart';

class ProductManager extends ChangeNotifier {
  ProductManager() {
    _loadAllProducts();
  }

  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  List<Product> allProducts = [];
  List<Product> filterProducts;
  StreamSubscription _subscription;

  String _search = '';
  String get search => _search;

  bool _grid = false;

  bool get isgrid => _grid;

  void changeTypeView(bool newBool) {
    _grid = !_grid;
    notifyListeners();
  }

  set search(String value) {
    _search = value;
    notifyListeners();
  }

  void onChangeSearchText(String searchText) {
    if (searchText == null) {
      filterProducts = allProducts;
      return;
    }
    filterProducts = allProducts
        .where((produto) => produto.name.toLowerCase().contains(searchText))
        .toList();
    notifyListeners();
  }

  void _loadAllProducts() {
    _subscription = firebaseFirestore.collection('produtos').snapshots().listen(
      (event) {
        allProducts.clear();
        for (final doc in event.docs) {
          allProducts.add(Product.fromDocument(doc));
        }
        filterProducts = allProducts;
        notifyListeners();
      },
    );
  }

  void update(Product produto) {
    allProducts.removeWhere((p) => p.id == produto.id);
    allProducts.add(produto);
    notifyListeners();
  }

  void delete(Product produto) {
    produto.delete(produto);
    allProducts.removeWhere((p) => p.id == produto.id);
    notifyListeners();
  }
}
