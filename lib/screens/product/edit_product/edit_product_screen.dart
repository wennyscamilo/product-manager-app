import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:productmanagerapp/models/product/product.dart';
import 'package:productmanagerapp/models/product/product_manager.dart.dart';

import 'package:productmanagerapp/themes/app_colors.dart';
import 'package:productmanagerapp/themes/app_text_styles.dart';
import 'package:productmanagerapp/widgets/custom_app_bar_home.dart';
import 'package:productmanagerapp/widgets/rounded_button.dart';
import 'package:provider/provider.dart';

import 'components/images_form.dart';

class EditProductScreen extends StatelessWidget {
  EditProductScreen(Product p)
      : editing = p != null,
        product = p != null ? p.clone() : Product();
  final Product product;
  final bool editing;

  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: product,
      child: NavegacaoTemplate(
        deleteProduct: () {
          context.read<ProductManager>().delete(product);
          Navigator.of(context).pop();
        },
        editing: editing,
        titulo: editing ? 'Editar Produto' : 'Criar Produto',
        bigContent: true,
        onBack: () {
          Navigator.pop(context);
        },
        conteudo: Form(
            key: formkey,
            child: Consumer<ProductManager>(
              builder: (_, productManager, __) {
                return ListView(
                  children: [
                    ImagesForm(product),
                    Divider(
                      color: AppColors.grey,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          TextFormField(
                            initialValue: product.name,
                            decoration: InputDecoration(
                              hintText: 'Título',
                              labelText: 'Título',
                              border: InputBorder.none,
                            ),
                            validator: (name) {
                              if (name.length < 6) return 'Título muito Curto';
                              return null;
                            },
                            onSaved: (name) => product.name = name,
                          ),
                          TextFormField(
                            initialValue: product.price?.toStringAsFixed(2),
                            decoration: InputDecoration(
                              hintText: 'Preço',
                              labelText: 'Preço',
                              prefixText: 'R\$',
                              border: InputBorder.none,
                            ),
                            keyboardType: const TextInputType.numberWithOptions(
                                decimal: true),
                            validator: (price) {
                              if (num.tryParse(price) == null)
                                return 'Inválido';
                              return null;
                            },
                            onSaved: (price) =>
                                product.price = num.tryParse(price),
                          ),
                          TextFormField(
                            initialValue: product.stock?.toString(),
                            decoration: const InputDecoration(
                              labelText: 'Estoque',
                              border: InputBorder.none,
                              isDense: true,
                            ),
                            keyboardType: TextInputType.number,
                            validator: (stock) {
                              if (int.tryParse(stock) == null)
                                return 'Inválido';
                              return null;
                            },
                            onChanged: (stock) =>
                                product.stock = int.tryParse(stock),
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            onSaved: (stock) =>
                                product.stock = int.tryParse(stock),
                          ),
                          TextFormField(
                            initialValue: product.description,
                            decoration: const InputDecoration(
                              hintText: 'Descrição',
                              labelText: 'Descrição',
                              border: InputBorder.none,
                            ),
                            maxLines: null,
                            validator: (desc) {
                              if (desc.length < 10)
                                return 'Descrição Muito Curta';
                              return null;
                            },
                            onSaved: (desc) => product.description = desc,
                          ),
                          Consumer<Product>(
                            builder: (_, product, __) {
                              return RoundedButton(
                                press: !product.loading
                                    ? () async {
                                        if (formkey.currentState.validate()) {
                                          formkey.currentState.save();

                                          await product.save();

                                          context
                                              .read<ProductManager>()
                                              .update(product);

                                          Navigator.of(context).pop();
                                        }
                                      }
                                    : null,
                                child: product.loading
                                    ? CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            Colors.white),
                                      )
                                    : Text(
                                        "Salvar",
                                        style: AppTextStyles.textButtonWhite,
                                      ),
                                textColor: Colors.white,
                                color: AppColors.blueLigth,
                              );
                            },
                          )
                        ],
                      ),
                    )
                  ],
                );
              },
            )),
      ),
    );
  }
}
