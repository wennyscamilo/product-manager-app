import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/product/product.dart';

import 'package:productmanagerapp/themes/app_colors.dart';

import 'image_source_sheet.dart';

class ImagesForm extends StatelessWidget {
  const ImagesForm(this.produto);
  final Product produto;
  @override
  Widget build(BuildContext context) {
    return FormField<List<dynamic>>(
      initialValue: List.from(produto.images),
      validator: (images) {
        if (images.isEmpty) return 'Insira ao menos uma  Imagem';
        return null;
      },
      onSaved: (images) => produto.newImages = images,
      builder: (state) {
        void onImageSelected(File file) {
          state.value.add(file);
          state.didChange(state.value);
          Navigator.of(context).pop();
        }

        return Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enableInfiniteScroll: false,
                enlargeCenterPage: true,
              ),
              items: state.value.map<Widget>((image) {
                return Container(
                  margin: EdgeInsets.all(5.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Stack(
                        children: <Widget>[
                          if (image is String)
                            Image.network(image,
                                fit: BoxFit.cover, width: 1000.0)
                          else
                            Image.file(image as File,
                                fit: BoxFit.cover, width: 1000.0),
                          Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                              icon: Icon(
                                Icons.remove_circle_rounded,
                                color: Colors.red,
                              ),
                              onPressed: () {
                                state.value.remove(image);
                                state.didChange(state.value);
                              },
                            ),
                          ),
                        ],
                      )),
                );
              }).toList()
                ..add(Material(
                  color: Colors.white,
                  child: IconButton(
                    icon: Icon(
                      Icons.add_a_photo,
                    ),
                    iconSize: 50,
                    onPressed: () {
                      if (Platform.isAndroid)
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => ImageSourceSheet(
                            onImageSelected: onImageSelected,
                          ),
                        );
                      else
                        showCupertinoModalPopup(
                          context: context,
                          builder: (context) => ImageSourceSheet(
                            onImageSelected: onImageSelected,
                          ),
                        );
                    },
                  ),
                )),
            ),
            if (state.hasError)
              Container(
                margin: const EdgeInsets.only(top: 16, left: 16),
                alignment: Alignment.centerLeft,
                child: Text(
                  state.errorText,
                  style: TextStyle(
                    color: AppColors.red,
                    fontSize: 12,
                  ),
                ),
              )
          ],
        );
      },
    );
  }
}
