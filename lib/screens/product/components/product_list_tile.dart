import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/product/product.dart';

import 'package:productmanagerapp/themes/app_text_styles.dart';

class ProductListTile extends StatelessWidget {
  const ProductListTile(this.product);

  final Product product;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed('/edit_product', arguments: product);
        },
        child: Container(
          child: Card(
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              height: 100,
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  AspectRatio(
                    aspectRatio: 1,
                    child: Image.network(product.images.first),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.name,
                          style: AppTextStyles.itemList,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4),
                          child: Text(
                            'Estoque: ${product.stock}',
                            style: AppTextStyles.hint,
                          ),
                        ),
                        Expanded(
                          child: Row(
                            children: [
                              Text(
                                "R\$ ${product.price.toStringAsFixed(2)}",
                                style: AppTextStyles.textPrice,
                              ),
                              Spacer(),
                              IconButton(
                                icon: Icon(
                                  Icons.edit,
                                  size: 15,
                                ),
                                onPressed: () {
                                  Navigator.of(context).pushNamed(
                                      '/edit_product',
                                      arguments: product);
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
