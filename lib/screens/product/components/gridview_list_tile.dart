import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/product/product.dart';

import 'package:productmanagerapp/themes/app_text_styles.dart';

class GridViewListTile extends StatelessWidget {
  const GridViewListTile(this.product);

  final Product product;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed('/edit_product', arguments: product);
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.5),
              offset: Offset(3, 2),
              blurRadius: 7,
            )
          ],
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: Image.network(
                    product.images.first,
                    width: double.infinity,
                  )),
            ),
            Flexible(
              child: Text(
                product.name,
                style: AppTextStyles.itemList,
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Flexible(
              child: Text(
                'Estoque: ${product.stock.toString()}',
                style: AppTextStyles.hint,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "R\$ ${product.price.toStringAsFixed(2)}",
                    style: AppTextStyles.textPrice,
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed('/edit_product', arguments: product);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
