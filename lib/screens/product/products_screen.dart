import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:productmanagerapp/models/product/product.dart';
import 'package:productmanagerapp/models/product/product_manager.dart.dart';
import 'package:productmanagerapp/models/user/usuario_manager.dart';

import 'package:productmanagerapp/themes/app_text_styles.dart';
import 'package:productmanagerapp/widgets/custom_app_bar_home.dart';
import 'package:productmanagerapp/widgets/custom_drawer.dart';
import 'package:provider/provider.dart';
import 'package:productmanagerapp/themes/app_images.dart';

import 'components/gridview_list_tile.dart';
import 'components/product_list_tile.dart';

class ProductsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      backgroundColor: Color(0xfff6f7f9),
      body: Consumer2<ProductManager, Usermanager>(
        builder: (_, productManager, userManager, __) {
          final products = productManager.filterProducts;

          return SingleChildScrollView(
            child: Column(
              children: [
                CustomAppBarHome(
                  onPressedLogout: () {
                    userManager.signOut();
                    Navigator.of(context).pushReplacementNamed('/base');
                  },
                  onChanged: productManager.onChangeSearchText,
                  onPressedclearText: () async {},
                ),
                SizedBox(
                  height: 20,
                ),
                if (products?.isEmpty ?? true)
                  Container(
                    height: 300,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AspectRatio(
                            aspectRatio: 2.5,
                            child: SvgPicture.asset(
                              AppImages.productAdd,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 60, right: 60),
                            child: Center(
                              child: Text(
                                productManager.allProducts.length >= 1
                                    ? 'Busca não encontrada'
                                    : 'Sem produtos adicionados no momento!',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: AppTextStyles.titleScreen,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                products.length <= 0
                    ? Container()
                    : IconButton(
                        icon: Icon(productManager.isgrid
                            ? Icons.grid_on
                            : Icons.grid_off),
                        onPressed: () {
                          productManager.changeTypeView(false);
                        }),
                productManager.isgrid
                    ? GridView.count(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        crossAxisCount: 2,
                        childAspectRatio: .63,
                        padding: const EdgeInsets.all(10),
                        mainAxisSpacing: 4.0,
                        crossAxisSpacing: 10,
                        children: products?.map((Product product) {
                              return GridViewListTile(product);
                            })?.toList() ??
                            [],
                      )
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: products.length,
                        itemBuilder: (_, index) {
                          return ProductListTile(products[index]);
                        },
                      )
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: Theme.of(context).primaryColor,
        onPressed: () {
          Navigator.of(context).pushNamed(
            '/edit_product',
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
