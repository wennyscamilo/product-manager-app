import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:productmanagerapp/helpers/validators.dart';
import 'package:productmanagerapp/models/user/usuario.dart';
import 'package:productmanagerapp/models/user/usuario_manager.dart';

import 'package:productmanagerapp/themes/app_text_styles.dart';
import 'package:productmanagerapp/widgets/custom_app_bar_home.dart';
import 'package:productmanagerapp/widgets/rounded_button.dart';
import 'package:productmanagerapp/widgets/rounded_input.dart';
import 'package:cpfcnpj/cpfcnpj.dart';

import 'package:provider/provider.dart';

class SingupScreen extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final Usuario usuario = Usuario();

  @override
  Widget build(BuildContext context) {
    return NavegacaoTemplate(
      titulo: 'Criar conta',
      bigContent: true,
      onBack: () {
        Navigator.pop(context);
      },
      conteudo: Center(
        child: Form(
          key: formKey,
          child: Consumer<Usermanager>(
            builder: (_, userManager, __) {
              return ListView(
                physics: ClampingScrollPhysics(),
                padding: const EdgeInsets.all(16),
                shrinkWrap: true,
                children: [
                  RoundedInput(
                    hintText: 'CPF',
                    keyboardType: TextInputType.number,
                    enabled: !userManager.loading,
                    validator: (cpf) {
                      if (cpf.isEmpty)
                        return 'Campo Obrigatório';
                      else if (!CPF.isValid(cpf)) return 'CPF inválido';
                      return null;
                    },
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      CpfInputFormatter()
                    ],
                    onSaved: (cpf) => usuario.cpf = cpf,
                  ),
                  RoundedInput(
                    hintText: 'Nome',
                    enabled: !userManager.loading,
                    validator: (name) {
                      if (name.isEmpty) {
                        return 'Campo Obrigatório';
                      }
                      return null;
                    },
                    onSaved: (nome) => usuario.name = nome,
                  ),
                  RoundedInput(
                    hintText: 'Número de telefone',
                    enabled: !userManager.loading,
                    validator: (tel) {
                      if (tel.isEmpty) return 'Campo Obrigatório';

                      return null;
                    },
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      TelefoneInputFormatter()
                    ],
                    onSaved: (cellPhone) => usuario.cellPhone = cellPhone,
                    keyboardType: TextInputType.phone,
                  ),
                  RoundedInput(
                    hintText: 'E-mail',
                    enabled: !userManager.loading,
                    keyboardType: TextInputType.emailAddress,
                    validator: (email) {
                      if (email.isEmpty)
                        return 'Campo Obrigatório';
                      else if (!emailValid(email)) return 'E-mail inválido';
                      return null;
                    },
                    onSaved: (email) => usuario.email = email,
                  ),
                  RoundedInput(
                    hintText: 'Senha',
                    enabled: !userManager.loading,
                    obscureText: true,
                    validator: (pass) {
                      if (pass.isEmpty)
                        return 'Campo Obrigatório';
                      else if (pass.length < 6) return 'Senha Muito Curta';
                      return null;
                    },
                    onSaved: (pass) => usuario.password = pass,
                  ),
                  RoundedInput(
                    hintText: 'Repita a Senha',
                    enabled: !userManager.loading,
                    obscureText: true,
                    validator: (pass) {
                      if (pass.isEmpty)
                        return 'Campo Obrigatório';
                      else if (pass.length < 6) return 'Senha Muito Curta';
                      return null;
                    },
                    onSaved: (confirmPass) =>
                        usuario.confirmPassword = confirmPass,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  RoundedButton(
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    press: userManager.loading
                        ? null
                        : () {
                            if (formKey.currentState.validate()) {
                              formKey.currentState.save();
                              if (usuario.password != usuario.confirmPassword) {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: const Text('Senhas não coincidem'),
                                  backgroundColor: Colors.red,
                                ));
                                return;
                              }
                              userManager.signup(
                                usuario: usuario,
                                onSucess: () {
                                  Navigator.of(context).pop();
                                },
                                onFail: (e) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Falhas ao cadastrar: $e'),
                                      backgroundColor: Colors.red,
                                    ),
                                  );
                                },
                              );

                              //user manager
                            }
                          },
                    child: userManager.loading
                        ? CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          )
                        : Text(
                            "Criar Conta",
                            style: AppTextStyles.textButtonWhite,
                          ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
