import 'package:flutter/material.dart';
import 'package:productmanagerapp/models/user/usuario_manager.dart';

import 'package:productmanagerapp/screens/login/login_screen.dart';
import 'package:productmanagerapp/screens/product/products_screen.dart';

import 'package:provider/provider.dart';

class BaseScreen extends StatelessWidget {
  const BaseScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<Usermanager>(
        builder: (_, userManager, __) {
          return userManager.isLoggedin ? ProductsScreen() : LoginScreen();
        },
      ),
    );
  }
}
