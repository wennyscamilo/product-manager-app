import 'package:flutter_svg/flutter_svg.dart';
import 'package:productmanagerapp/helpers/validators.dart';
import 'package:productmanagerapp/models/user/usuario.dart';
import 'package:productmanagerapp/models/user/usuario_manager.dart';

import 'package:productmanagerapp/screens/product/products_screen.dart';

import 'package:productmanagerapp/themes/app_images.dart';
import 'package:productmanagerapp/themes/app_text_styles.dart';

import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import '../../widgets/rounded_button.dart';
import '../../widgets/rounded_input.dart';
import 'widgets/no_account_widget.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailController = TextEditingController();

  final TextEditingController passController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  bool _showPass = true;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: formKey,
          child: Consumer<Usermanager>(
            builder: (_, userManager, __) {
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        'Login',
                        style: AppTextStyles.titleLogin,
                      ),
                    ),
                    SizedBox(height: size.height * 0.03),
                    SvgPicture.asset(
                      AppImages.loginPage,
                      height: size.height * 0.35,
                    ),
                    SizedBox(height: size.height * 0.03),
                    RoundedInput(
                      hintText: 'Email',
                      controller: emailController,
                      iconData: Icons.person,
                      enabled: !userManager.loading,
                      keyboardType: TextInputType.emailAddress,
                      validator: (email) {
                        if (!emailValid(email)) return "E-mail inválido";
                        return null;
                      },
                    ),
                    RoundedInput(
                      enabled: !userManager.loading,
                      controller: passController,
                      obscureText: _showPass,
                      hintText: 'Senha',
                      isInputSenha: true,
                      iconData: Icons.lock,
                      validator: (pass) {
                        if (pass.isEmpty || pass.length < 6) {
                          return 'Senha Invalida';
                        }
                        return null;
                      },
                      onPressed: () {
                        setState(() {
                          _showPass = !_showPass;
                        });
                      },
                    ),
                    RoundedButton(
                      text: "LOGIN",
                      press: userManager.loading
                          ? null
                          : () {
                              if (formKey.currentState.validate()) {
                                userManager.signIn(
                                    usuario: Usuario(
                                        email: emailController.text,
                                        password: passController.text),
                                    onFail: (e) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content: Text("Falha ao Entrar: $e"),
                                          backgroundColor: Colors.red,
                                        ),
                                      );
                                    },
                                    onSuccess: () {
                                      Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ProductsScreen()),
                                        (Route<dynamic> route) => false,
                                      );
                                    });
                              }
                            },
                      child: userManager.loading
                          ? CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.white),
                            )
                          : Text(
                              "Entrar",
                              style: AppTextStyles.textButtonWhite,
                            ),
                    ),
                    SizedBox(height: size.height * 0.03),
                    NoAcconuntWidget(
                      press: () {
                        Navigator.of(context).pushNamed('/cadastro');
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
